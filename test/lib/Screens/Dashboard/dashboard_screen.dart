// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:test/Screens/EditProfile/edit_profile.dart';
import 'package:test/Screens/Features/buy_livestock.dart';
import 'package:test/Screens/Features/connect_farmers.dart';
import 'package:test/Screens/Login/login_screen.dart';
import 'package:test/components/dashboard_button.dart';
import 'package:test/components/rounded_button.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return EditProfile();
              },
            ),
          );
        },
        child: SizedBox(
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return EditProfile();
                  },
                ),
              );
            },
            child: Icon(
              Icons.person,
            ),
          ),
        ),
      ),
      backgroundColor: Colors.green,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(Icons.menu, color: Colors.white, size: 30.0),
                  SizedBox(
                    child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Icon(Icons.person)),
                  )
                ],
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  "Dashboard",
                  style: TextStyle(
                      color: Colors.amber,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Raleway'),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: Wrap(
                  spacing: 20.0,
                  runSpacing: 20.0,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return BuyLiveStockScreen();
                            },
                          ),
                        );
                      },
                      child: SizedBox(
                        width: 160,
                        height: 160,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              height: 200,
                              width: 500,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage('assets/images/buy.jpg'),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                              // child: Text(
                              //   "Buy Livestock",
                              //   style: TextStyle(
                              //     color: Colors.black,
                              //     fontWeight: FontWeight.bold,
                              //     fontSize: 20,
                              //   ),
                              //   textAlign: TextAlign.center,
                              // ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return ConnectFarmersScreen();
                            },
                          ),
                        );
                      },
                      child: SizedBox(
                        width: 160,
                        height: 160,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              height: 200,
                              width: 500,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage('assets/images/farm.png'),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                              // child: Text(
                              //   "Buy Livestock",
                              //   style: TextStyle(
                              //     color: Colors.black,
                              //     fontWeight: FontWeight.bold,
                              //     fontSize: 20,
                              //   ),
                              //   textAlign: TextAlign.center,
                              // ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
