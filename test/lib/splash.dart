// import 'package:flutter/material.dart';
// import 'package:test/Screens/Welcome/welcome_screen.dart';

// class Splash extends StatefulWidget {
//   const Splash({Key? key}) : super(key: key);

//   @override
//   State<Splash> createState() => _SplashState();
// }

// class _SplashState extends State<Splash> {
//   @override
//   void initState() {
//     super.initState();
//     _navigateHome();
//   }

//   _navigateHome() async {
//     await Future.delayed(Duration(milliseconds: 1500), () {});
//     Navigator.pushReplacement(context,
//         MaterialPageRoute(builder: (context) => const WelcomeScreen()));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Container(
//               height: 120,
//               width: 120,
//               decoration: BoxDecoration(
//                 image: DecorationImage(
//                   image: NetworkImage('assets/images/farmers.png'),
//                   fit: BoxFit.contain,
//                 ),
//                 borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                 border: Border.all(
//                   color: Colors.black,
//                   width: 2.0,
//                 ),
//               ),
//             ),
//             Container(
//               child: Text(
//                 "LIVESTOCK TRADE",
//                 style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
