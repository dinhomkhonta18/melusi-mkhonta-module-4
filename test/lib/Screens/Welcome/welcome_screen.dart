// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:test/Screens/Dashboard/dashboard_screen.dart';
import 'package:test/Screens/Login/login_screen.dart';
import 'package:test/Screens/Welcome/components/body.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // ignore: prefer_const_constructors
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return DashboardScreen();
              },
            ),
          );
        },
        child: Icon(Icons.dashboard),
      ),
      body: Body(),
    );
  }
}
